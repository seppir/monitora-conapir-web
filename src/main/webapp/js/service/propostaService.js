angular
	.module('app')
	.service('propostaService', ['$http', '$q', propostaService]);

function propostaService($http, $q) {

	this.findAllResolutions = findAllResolutions;
	this.findPriority = findPriority;
	this.findWithText = findWithText;
	this.findByOption = findByOption;
	this.findLastChanges = findLastChanges;
	this.findDetails = findDetails;

	function findAllResolutions() {
		var deferred = $q.defer();
		$http
			.post('/proposta/buscar')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findPriority() {
		var deferred = $q.defer();
		$http
			.post('/proposta/buscar', {
				prioritarias: true
			})
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findWithText(text) {
		var deferred = $q.defer();
		$http
			.post('/proposta/buscar', {
				filtro: text
			})
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByOption(optionId) {
		var deferred = $q.defer();
		$http
			.get('/proposta/poropcao/' + optionId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findLastChanges(shorten) {
		var deferred = $q.defer();
		$http
			.get('/proposta/ultimasalteracoes?resumido=' + (shorten || false))
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findDetails(resolutionId) {
		var deferred = $q.defer();
		$http
			.get('/proposta/' + resolutionId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
