angular
	.module('app')
	.service('tagCloudService', ['$http', '$q', tagCloudService]);

function tagCloudService($http, $q) {

	this.findWords = findWords;

	function findWords() {
		var deferred = $q.defer();
		$http
			.get('/termobusca/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
