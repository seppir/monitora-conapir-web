angular
	.module('app')
	.service('anexoService', ['$http', '$q', anexoService]);

function anexoService($http, $q) {
	this.findByOccurrence = findByOccurrence;

	function findByOccurrence(occurrenceId) {
		var deferred = $q.defer();
		$http
			.get('/anexo/ocorrencia/' + occurrenceId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
