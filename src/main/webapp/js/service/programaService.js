angular
	.module('app')
	.service('programaService', ['$http', '$q', programaService]);

function programaService($http, $q) {

	this.findAll = findAll;
	this.findOne = findOne;
	this.countResolutions = countResolutions;

	function findOne(id) {
		var deferred = $q.defer();
		$http
			.get('/programa/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('/programa/todos')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function countResolutions() {
		var deferred = $q.defer();
		$http
			.get('/programa/contar')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
