angular
	.module('app')
	.service('opcaoService', ['$http', '$q', opcaoService]);

function opcaoService($http, $q) {

	this.findAllAndSet = findAllAndSet;
	this.findAll = findAll;
	this.findOne = findOne;
	this.findByObjective = findByObjective;
	this.countResolutions = countResolutions;

	function findAllAndSet(ids, ocorrencia) {
		return this
			.findAll(ids)
			.then(function(opcoes) {
				ocorrencia.programas = arvoreOpcoes(opcoes);
			});

		function arvoreOpcoes(opcoes) {
			var programas = {};

			var opcao;
			for(var i in opcoes) {
				opcao = opcoes[i];

				// verifica se possui esse programa
				if(!programas[opcao.programa.id]) {
					programas[opcao.programa.id] = {
						descricao: opcao.programa.descricao,
						objetivos: {}
					};
				}

				// verifica se possui esse objetivo
				var programa = programas[opcao.programa.id];
				if(!programa.objetivos[opcao.objetivo.id]) {
					programa.objetivos[opcao.objetivo.id] = {
						descricao: opcao.objetivo.descricao,
						orgao: opcao.orgao,
						metasIniciativas: {}
					};
				}

				// verifica se possui essa meta/iniciativa
				var objetivo = programa.objetivos[opcao.objetivo.id];
				if(!objetivo.metasIniciativas[opcao.id]) {
					objetivo.metasIniciativas[opcao.id] = opcao;
				}
			}

			return programas;
		}
	}

	function findAll(ids) {
		var deferred = $q.defer();
		$http
			.post('/opcao/todas', {ids: ids})
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findOne(id) {
		var deferred = $q.defer();
		$http
			.get('/opcao/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByObjective(idObjetivo) {
		var deferred = $q.defer();
		$http
		.get('/opcao/porobjetivo/' + idObjetivo)
		.success(function(data) {
			deferred.resolve(data);
		});
		return deferred.promise;
	}

	function countResolutions(ids) {
		var deferred = $q.defer();
		$http
			.post('/opcao/contar', {ids: ids})
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
