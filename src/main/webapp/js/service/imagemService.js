angular
	.module('app')
	.service('imagemService', ['$http', '$q', imagemService]);

function imagemService($http, $q) {

	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('/imagem/todas')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
