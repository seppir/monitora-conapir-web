angular
	.module('app')
	.service('politicaService', ['$http', '$q', politicaService]);

function politicaService($http, $q) {

	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('/politica/todas')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
