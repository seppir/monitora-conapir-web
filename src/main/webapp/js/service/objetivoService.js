angular
	.module('app')
	.service('objetivoService', ['$http', '$q', objetivoService]);

function objetivoService($http, $q) {

	this.findByProgram = findByProgram;
	this.countResolutions = countResolutions;

	function findByProgram(idPrograma) {
		var deferred = $q.defer();
		$http
			.get('/opcao/porprograma/' + idPrograma)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function countResolutions(ids) {
		var deferred = $q.defer();
		$http
			.post('/objetivo/contar', {ids: ids})
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
