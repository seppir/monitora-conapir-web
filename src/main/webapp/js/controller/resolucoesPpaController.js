angular
    .module('app')
    .controller('ResolucoesPpaController', ['$scope', '$location', 'opcaoService', 'propostaService', 'programaService', ResolucoesPpaController]);

function ResolucoesPpaController($scope, $location, opcaoService, propostaService, programaService) {

	var params = $location.search();
	$scope.eixo = params.e;
	$scope.programa = params.p;
	$scope.objetivo = params.o;
	$scope.meta = params.m;

	programaService
		.findOne($scope.programa)
		.then(function(data) {
			$scope.programaSelecionado = data;
		});

	opcaoService
		.findOne($scope.meta)
		.then(function(data) {
			$scope.opcao = data;
		});

	propostaService
		.findByOption($scope.meta)
		.then(function(data) {
			$scope.propostas = data;
		});

}

