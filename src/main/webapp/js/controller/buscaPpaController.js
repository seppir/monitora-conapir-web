angular
	.module('app')
	.controller('BuscaPpaController', ['$scope', '$location', 'programaService', 'imagemService', BuscaPpaController]);

function BuscaPpaController($scope, $location, programaService, imagemService) {

	$scope.subtema = [];

	$scope.initSubtema = function($first, descPpa, subtema1) {
		if($first) {
			$scope.subtema[descPpa] = subtema1;
		}
	};

	$scope.ativaSubtema = function(descPpa, subtema) {
		$scope.subtema[descPpa] = subtema;
	};

	$scope.listaObjetivos = function(eixo, programa) {
		$location
			.path('/objetivos')
			.search({
			    e: eixo,
			    p: programa
			});
	};

	$scope.classeImagem = function(politicas, ativo, $first) {
		var classes = '';

		if($first) {
			classes += ' col-md-offset-2';
		}
		if(ativo) {
			classes += ' ativo';
		}

		//col-md-2 col-sm-3 col-xs-6
		//{ativo : subtema[descPpa] == idPolitica, 'col-md-offset-2' : $first}

		var qtd = Object.keys(politicas).length;
		classes += ' col-md-' + 8 / qtd;
		classes += ' col-sm-' + 24 / qtd;
		classes += ' col-xs-' + 24 / qtd;

		return classes;
	};

	$scope.classeColuna = function(assuntos) {
		var col = 12 / Object.keys(assuntos).length;
		return 'col-md-' + col + ' col-sm-' + col;
	};

	$scope.getImagem = function(id) {
		if($scope.imagens) {
			var img = binarySearch($scope.imagens, id, 'id');
			if(img) {
				var ext = img.nomeArquivo.substring(img.nomeArquivo.indexOf('.') + 1);
				return 'data:' + ext + ';base64,' + img.arquivoBase64;
			} else {
				// Imagem nao encontrada
				return 'img/NAO_ACHOU.png';
			}
		}
	};

	programaService
		.findAll()
		.then(function(data) {
			$scope.ppas = data;
		});

	programaService
		.countResolutions()
		.then(function(data) {
			$scope.cont = data;
		});

	imagemService
		.findAll()
		.then(function(data) {
			$scope.imagens = data;
		});

}
