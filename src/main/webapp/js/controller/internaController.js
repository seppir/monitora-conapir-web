angular
    .module('app')
    .controller('InternaController', ['$scope', '$window', '$location', 'propostaService', 'opcaoService', 'hotkeys', InternaController]);

function InternaController($scope, $window, $location, propostaService, opcaoService, hotkeys) {

	$scope.buscando = true;
	$scope.filtro = null;
	$scope.tipoFiltro = null;

	$scope.opcoesCarregadas = false;

	$scope.subtema = 0;

	$scope.ativaSubtema = function(subtema) {
		if($scope.subtema & subtema) {
			$scope.subtema &= ~subtema;
		} else {
			$scope.subtema |= subtema;
		}
	}

	$scope.inclui = function(subtema) {
		return ($scope.subtema & subtema) != 0;
	};

	$scope.exportar = function(tipo) {
		var p = 'filtro=' + $scope.tipoFiltro + '&subtema=' + $scope.subtema;
		if($scope.query) {
			p += '&texto=' + removeDiacritics($scope.query);
		}
		$window.open('proposta/pdf?' + p, '_blank');
	}

	$scope.getTipo = function(opcao) {
		return opcao.tipo == 'META' ? 'Meta' : 'Iniciativa';
	}

	$scope.abrirFechar = function(item) {
		item.aberto = !item.aberto;
	}

	$scope.classeBotaoArvore = function(item) {
		return item.aberto ? 'glyphicon-folder-open' : 'glyphicon-folder-close';
	}

	hotkeys.bindTo($scope)
		.add({
			combo: '1',
			callback: function() {
				$scope.ativaSubtema(1);
			}
		})
		.add({
			combo: '2',
			callback: function() {
				$scope.ativaSubtema(2);
			}
		})
		.add({
			combo: '3',
			callback: function() {
				$scope.ativaSubtema(4);
			}
		})
		.add({
			combo: '4',
			callback: function() {
				$scope.ativaSubtema(8);
			}
		})
		.add({
			combo: 'ctrl+alt+b',
			callback: function() {
				angular.element('#busca').focus();
			}
		});

	function filtrarPorSubtema(propostas) {
		var filtro = [[], [], [], []];
		for(var index in propostas) {
			try {
				filtro[propostas[index].idEixo - 1].push(propostas[index]);
			} catch(e) {
				console.log(e);
			}
		}
		$scope.propostas = filtro;

		if($scope.opcoes) {
			initOpcoes();
		}

		$scope.buscando = false;
	}

	function initOpcoes() {
		for(var i in $scope.propostas) {
			var subtema = $scope.propostas[i];
			for(var j in subtema) {
				var proposta = subtema[j];
				proposta.ppasLength = 0;
				proposta.ppas = {};
				var opcoes = [];

				// populando opcoes
				for(var k in proposta.ocorrencias) {
					var ocorrencia = proposta.ocorrencias[k];
					for(var l in ocorrencia.idOpcoes) {
						opcoes.push(binarySearch($scope.opcoes, ocorrencia.idOpcoes[l], 'id'));
					}
				}

				var opcao;
				for(var k in opcoes) {
					opcao = opcoes[k];

					// verifica se possui esse ppa
					if(!proposta.ppas[opcao.ppa.id]) {
						proposta.ppasLength++;
						proposta.ppas[opcao.ppa.id] = {
							ppa: opcao.ppa.descricao,
							programas: {}
						};
					}

					// verifica se possui esse programa
					var ppa = proposta.ppas[opcao.ppa.id];
					if(!ppa.programas[opcao.programa.id]) {
						ppa.programas[opcao.programa.id] = {
							programa: opcao.programa.descricao,
							objetivos: {}
						};
					}

					// verifica se possui esse objetivo
					var programa = ppa.programas[opcao.programa.id];
					if(!programa.objetivos[opcao.objetivo.id]) {
						programa.objetivos[opcao.objetivo.id] = {
							objetivo: opcao.objetivo.descricao,
							metasIniciativas: {}
						};
					}

					// verifica se possui essa meta/iniciativa
					var objetivo = programa.objetivos[opcao.objetivo.id];
					if(!objetivo.metasIniciativas[opcao.id]) {
						objetivo.metasIniciativas[opcao.id] = opcao;
					}
				}
			}
		}

		$scope.opcoesCarregadas = true;
	}

	var params = $location.search();

	if(params.hasOwnProperty('p') && params.p) {
		propostaService
			.findPriority()
			.then(filtrarPorSubtema);
		$scope.tipoFiltro = 'PRIORITARIAS';
	} else if(params.hasOwnProperty('b') && params.b) {
		$scope.query = params.b;
		propostaService
			.findWithText($scope.query)
			.then(filtrarPorSubtema);
		$scope.tipoFiltro = 'TODAS';
	} else {
		propostaService
			.findAllResolutions()
			.then(filtrarPorSubtema);
		$scope.tipoFiltro = 'TODAS';
	}

	$scope.subtemas = [];
	for(var i = 0; i < 4; i++) {
		$scope.subtemas[i] = $scope.getDadosSubtema(i + 1);
	}

	opcaoService
		.findAll()
		.then(function(opcoes) {
			$scope.opcoes = opcoes;
			if($scope.propostas) {
				initOpcoes();
			}
		});

}
