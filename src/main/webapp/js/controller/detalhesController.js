angular
    .module('app')
    .controller('DetalhesController', ['$scope', 'idProposta', '$modalInstance', '$window', 'propostaService', 'opcaoService', 'anexoService', DetalhesController]);

function DetalhesController($scope, idProposta, $modalInstance, $window, propostaService, opcaoService, anexoService) {

	$scope.close = function() {
		$modalInstance.dismiss('cancel');
	};

	$scope.exportar = function(tipo) {
		$window.open('./proposta/' + $scope.proposta.id + '/pdf', '_blank');
	};

	if(idProposta) {
		propostaService
			.findDetails(idProposta)
			.then(initScreen);
	}

	function initScreen(proposta) {
		$scope.proposta = proposta;

		$scope.subtema = $scope.getTextoSubtema($scope.proposta.idEixo);
		$scope.prioritaria = $scope.proposta.prioritaria ? 'SIM' : 'NÃO';

		for(var i in $scope.proposta.ocorrencias) {
			var ocorrencia = $scope.proposta.ocorrencias[i];

			if(ocorrencia.idOpcoes && ocorrencia.idOpcoes.length) {
				ocorrencia.temOpcoes = true;
				opcaoService.findAllAndSet(ocorrencia.idOpcoes, ocorrencia);
			} else {
				ocorrencia.temOpcoes = false;
				ocorrencia.opcoes = [];
			}

			anexoService
				.findByOccurrence(ocorrencia.id)
				.then(function(anexos) {
					ocorrencia.anexos = anexos;
				});
		}
	}

}
