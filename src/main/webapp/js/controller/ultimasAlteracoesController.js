angular
    .module('app')
    .controller('UltimasAlteracoesController', ['$scope', 'propostaService', UltimasAlteracoesController]);

function UltimasAlteracoesController($scope, propostaService) {

	propostaService
		.findLastChanges()
		.then(function(data) {
			$scope.propostas = data;
			angular.element('#div_tables').css('display', 'block');
		});

}

