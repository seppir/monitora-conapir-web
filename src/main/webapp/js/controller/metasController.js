angular
    .module('app')
    .controller('MetasController', ['$scope', '$location', 'opcaoService', 'programaService', MetasController]);

function MetasController($scope, $location, opcaoService, programaService) {

	var params = $location.search();
	$scope.eixo = params.e;
	$scope.programa = params.p;
	$scope.idObjetivo = params.o;

	opcaoService
		.findByObjective($scope.idObjetivo)
		.then(function(data) {
			$scope.objetivos = filtrarPorTipo(data);
			$scope.textoObjetivo = data[0].objetivo.descricao;

			var ids = [];
			for(var i in data) {
				ids.push(data[i].id);
			}
			opcaoService
				.countResolutions(ids)
				.then(function(data) {
					$scope.cont = data;
				});
		});

	programaService
		.findOne($scope.programa)
		.then(function(data) {
			$scope.programaSelecionado = data;
		});

	$scope.navegaResolucoes = function(idMeta) {
		$location
			.path('/resolucoesppa')
			.search({
				e: $scope.eixo,
				p: $scope.programa,
				o: $scope.idObjetivo,
				m: idMeta
			});
	}

	function filtrarPorTipo(objetivos) {
		var iniciativas = [];
		var metas = [];
		for(var i in objetivos) {
			var obj = objetivos[i];
			if(obj.tipo == 'INICIATIVA') {
				iniciativas.push(obj);
			} else {
				metas.push(obj);
			}
		}
		return {
			iniciativas: iniciativas,
			metas: metas
		};
	}

}

