angular
    .module('app')
    .controller('HomeController', ['$scope', '$location', 'propostaService', 'tagCloudService', 'hotkeys', HomeController]);

function HomeController($scope, $location, propostaService, tagCloudService, hotkeys) {

	const TAG_CLOUD_HEIGHT = 178;

	$scope.navega = function() {
		$location
			.path('/interna')
			.search('b', $scope.filtro);
	}

	$scope.verTodasAlteracoes = function() {
		$location.path('ultimasalteracoes');
	}

	propostaService
		.findLastChanges(true)
		.then(function(data) {
			$scope.ultimasAlteracoes = data;
		});

	tagCloudService
		.findWords()
		.then(function(palavras) {
			$scope.sugestoesBusca = palavras;
			var words = [];
			var max = Math.min(palavras.length - 1, 10);

			for(var i = 0; i < max; i++) {
				var palavra = palavras[i];
				words.push({
					text: palavra.valor,
					weight: palavra.buscas,
					link: '#/interna?b=' + palavra.valor
				});
			}

			angular
				.element(document.getElementById('tagCloud'))
				.jQCloud(words, {
					height: TAG_CLOUD_HEIGHT,
					autoResize: true,
					fontSize: {
						from: 0.05,
						to: 0.01
					}
				});
		});

	hotkeys.bindTo($scope)
		.add({
			combo: 'ctrl+alt+b',
			callback: function() {
				angular.element('#busca').focus();
			}
		})
		.add({
			combo: 'ctrl+alt+r',
			callback: function() {
				angular.element('#btn_prioritarias').click();
			}
		})
		.add({
			combo: 'ctrl+alt+t',
			callback: function() {
				angular.element('#btn_todas').click();
			}
		})
		.add({
			combo: 'ctrl+alt+p',
			callback: function() {
				angular.element('#btn_ppa').click();
			}
		});

}

