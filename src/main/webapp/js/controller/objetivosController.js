angular
    .module('app')
    .controller('ObjetivosController', ['$scope', '$location', 'objetivoService', 'programaService', ObjetivosController]);

function ObjetivosController($scope, $location, objetivoService, programaService) {

	var params = $location.search();
	$scope.eixo = params.e;
	$scope.programa = params.p;

	programaService
		.findOne($scope.programa)
		.then(function(data) {
			$scope.programaSelecionado = data;
		});

	objetivoService
		.findByProgram($scope.programa)
		.then(function(data) {
			$scope.objetivos = data;

			var ids = [];
			for(var i in data) {
				ids.push(data[i].id);
			}

			objetivoService
				.countResolutions(ids)
				.then(function(data) {
					$scope.cont = data;
				});
		});

	$scope.navegaMetas = function(idObjetivo) {
		$location
			.path('/metas')
			.search({
				e: $scope.eixo,
				p: $scope.programa,
				o: idObjetivo
			});
	}

}

