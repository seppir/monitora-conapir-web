angular
	.module('app', ['ngRoute', 'ngAria', 'ngCookies', 'ui.bootstrap', 'cfp.hotkeys'])
	.config(['$httpProvider', '$routeProvider', '$compileProvider', 'hotkeysProvider', function($httpProvider, $routeProvider, $compileProvider, hotkeysProvider) {
		$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data):/);

		hotkeysProvider.useNgRoute = false;

		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$httpProvider.defaults.transformRequest = function(data) {
	        if (data === undefined) {
	            return data;
	        }
	        return $.param(data);
	    }
		$httpProvider.interceptors.push(function ($q) {
            return {
                request: function (config) {
                    config.url = ('/monitoraconapir/' + config.url).replace('//', '/');
                    return config || $q.when(config);
                }
            }
        });

		if(!detalhes) {
			$routeProvider
				.when('/', {
					templateUrl: './partials/home.html'
				})
				.when('/ultimasalteracoes', {
					templateUrl: './partials/ultimasalteracoes.html'
				})
				.when('/interna', {
					templateUrl: './partials/interna.html'
				})
				.when('/buscappa', {
					templateUrl: './partials/buscappa.html'
				})
				.when('/objetivos', {
					templateUrl: './partials/objetivos.html'
				})
				.when('/metas', {
					templateUrl: './partials/metas.html'
				})
				.when('/resolucoesppa', {
					templateUrl: './partials/resolucoesppa.html'
				})
				.when('/apresentacao',  {
					templateUrl: './partials/apresentacao.html'
				})
				.when('/entenda',  {
					templateUrl: './partials/entenda.html'
				})
				.when('/faleconosco',  {
					templateUrl: './partials/faleconosco.html'
				})
				.otherwise({
					redirectTo: '/'
				});
		}
	}])
	.run(['$window', '$rootScope', '$sce', '$cookies', '$location', 'hotkeys', '$modal', function($window, $rootScope, $sce, $cookies, $location, hotkeys, $modal) {

		var c = $cookies.get('highContrast');
		if(c === undefined) {
			$cookies.put(
				'highContrast',
				c = false
			);
		}
		$rootScope.highContrast = c === 'true';

		$rootScope.toggleContrast = function() {
			$cookies.put(
				'highContrast',
				$rootScope.highContrast = !$rootScope.highContrast
			);
		}

		$rootScope.textAsHtml = function(text) {
			return $sce.trustAsHtml(angular.element('<div/>').html(text).text());
		};

		$rootScope.mostrarDetalhes = function(idProposta) {
			$modal.open({
				templateUrl: './views/partials/templates/detalhes.tpl.html',
				controller: 'DetalhesController',
				size: 'lg',
				resolve: {
					idProposta: function() {
						return idProposta;
					}
				}
			});
		};

		$rootScope.getDadosSubtema = function(subtema) {
			var s = {
				textoCompleto: null,
				textoResumido: null,
				numero: subtema
			};

			switch(subtema) {
	            case 1:
		            s.textoCompleto = $rootScope.textAsHtml('1 - Estrat&eacute;gias para o desenvolvimento e o enfrentamento ao racismo');
		            s.textoResumido = $rootScope.textAsHtml('Desenvolvimento e enfrentamento ao racismo');
		            break;

	            case 2:
	            	s.textoCompleto = $rootScope.textAsHtml('2 - Pol&iacute;ticas de Igualdade Racial no Brasil: avan&ccedil;os e desafios');
	            	s.textoResumido = $rootScope.textAsHtml('Pol&iacute;ticas de Igualdade Racial no Brasil');
	            	break;

	            case 3:
	            	s.textoCompleto = $rootScope.textAsHtml('3 - Arranjos Institucionais para a sustentabilidade das Pol&iacute;ticas de Igualdade Racial');
	            	s.textoResumido = $rootScope.textAsHtml('Arranjos Institucionais');
	            	break;

	            case 4:
	            	s.textoCompleto = $rootScope.textAsHtml('4 - Participa&ccedil;&atilde;o pol&iacute;tica e controle social: igualdade racial nos espa&ccedil;os de decis&atilde;o');
	            	s.textoResumido = $rootScope.textAsHtml('Participa&ccedil;&atilde;o pol&iacute;tica e controle social');
	            	break;

	            default:
		            break;
            }

			return s;
		};

		$rootScope.getTextoSubtema = function(subtema) {
			var texto;
			switch(subtema) {
		        case 1:
			        texto = '1 - Estrat&eacute;gias para o desenvolvimento e o enfrentamento ao racismo';
			        break;

		        case 2:
			        texto = '2 - Pol&iacute;ticas de igualdade racial no Brasil: avan&ccedil;os e desafios';
			        break;

		        case 3:
			        texto = '3 - Arranjos Institucionais para assegurar a sustentabilidade das pol&iacute;ticas de igualdade racial: Sinapir, &oacute;rg&atilde;os de promo&ccedil;&atilde;o da igualdade racial, f&oacute;rum de gestores, conselhos e ouvidorias';
			        break;

		        case 4:
			        texto = '4 - Participa&ccedil;&atilde;o pol&iacute;tica e controle social: igualdade racial nos espa&ccedil;os de decis&atilde;o e mecanismos de participa&ccedil;&atilde;o da sociedade civil no monitoramento das pol&iacute;ticas de igualdade racial';
			        break;

		        default:
			        texto = '';
		        break;
	        }
			return $rootScope.textAsHtml(texto);
		};

		if(!detalhes) {
			hotkeys.bindTo($rootScope)
				.add({
					combo: 'i',
					callback: function() {
						$location.path('/');
					}
				})
				.add({
					combo: 'a',
					callback: function() {
						$location.path('/apresentacao');
					}
				})
				.add({
					combo: 'e',
					callback: function() {
						$location.path('/entenda');
					}
				})
				.add({
					combo: 'f',
					callback: function() {
						$location.path('/faleconosco');
					}
				})
				.add({
					combo: 'r',
					callback: function() {
						$window.open('http://sgp.seppir.gov.br/sar', '_blank');
					}
				})
				.add({
					combo: 'c',
					callback: $rootScope.toggleContrast
				});
		}

	}])
	.filter('ignoreDiacritics', function() {
		return function(list, search, attrs) {
			if(search) {
				var filtered = [];
				var query = removeDiacritics(search.trim()).toLowerCase();
				var item;

				outer: for(var i in list) {
					item = list[i];
					for(var j in attrs) {
						if(removeDiacritics(item[attrs[j]].toString().trim()).toLowerCase().indexOf(query) !== -1) {
							filtered.push(item);
							continue outer;
						}
					}
				}

				return filtered;
			}
			return list;
		}
	});
