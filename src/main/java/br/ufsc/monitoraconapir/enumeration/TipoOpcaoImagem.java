package br.ufsc.monitoraconapir.enumeration;

public enum TipoOpcaoImagem {

	META('M'), INICIATIVA('I'), OBJETIVO('O'), PROGRAMA('P'), ASSUNTO('N'), POLITICA('T'), PPA('A');

	private char sigla;

	private TipoOpcaoImagem(final char sigla) {
		this.sigla = sigla;
	}

	public char getSigla() {
		return sigla;
	}

	public static TipoOpcaoImagem converterDoBanco(final char dbData) {
		switch(dbData) {
			case 'M':
				return META;

			case 'I':
				return INICIATIVA;

			case 'O':
				return OBJETIVO;

			case 'P':
				return PROGRAMA;

			case 'N':
				return ASSUNTO;

			case 'T':
				return POLITICA;

			case 'A':
				return PPA;

			default:
				return null;
		}
	}

}
