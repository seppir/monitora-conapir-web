package br.ufsc.monitoraconapir.enumeration;

public enum TipoOpcao {

	META("Meta"), INICIATIVA("Iniciativa");

	private String descricao;

	private TipoOpcao(final String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoOpcao converterDoBanco(final String dbData) {
		switch(dbData) {
			case "I":
				return INICIATIVA;

			case "M":
				return META;

			default:
				return null;
		}
	}

}
