package br.ufsc.monitoraconapir.service;

import java.util.List;
import java.util.Map;

public interface OpcaoService {

	Map<Integer, Integer> countResolutions(final List<Integer> ids);

}
