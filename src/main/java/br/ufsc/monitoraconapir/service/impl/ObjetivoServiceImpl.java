package br.ufsc.monitoraconapir.service.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraconapir.model.Objetivo;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.service.ObjetivoService;

@Service
public class ObjetivoServiceImpl implements ObjetivoService {

	@Autowired
	private OpcaoRepository repository;

	@Override
	public Map<Integer, Integer> countResolutions(final List<Integer> ids) {
		final List<Objetivo> objetivos = repository.findObjectives(ids);
		final Map<Integer, Integer> contador = new TreeMap<>();
		for(final Objetivo objetivo : objetivos) {
			contador.put(objetivo.getId(), repository.countObjectiveResolutions(objetivo.getId()));
		}
		return contador;
	}

}
