package br.ufsc.monitoraconapir.service.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraconapir.model.Opcao;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.service.OpcaoService;

@Service
public class OpcaoServiceImpl implements OpcaoService {

	@Autowired
	private OpcaoRepository repository;

	@Override
	public Map<Integer, Integer> countResolutions(final List<Integer> ids) {
		final List<Opcao> opcoes = repository.findAll(ids);
		final Map<Integer, Integer> contador = new TreeMap<>();
		for(final Opcao opcao : opcoes) {
			contador.put(opcao.getId(), repository.countOptionResolutions(opcao.getId()));
		}
		return contador;
	}

}
