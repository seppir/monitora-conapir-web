package br.ufsc.monitoraconapir.service.impl;

import static br.ufsc.monitoraconapir.util.pdf.PdfUtils.FONTE_NEGRITO;
import static br.ufsc.monitoraconapir.util.pdf.PdfUtils.FONTE_TITULO;
import static br.ufsc.monitoraconapir.util.pdf.PdfUtils.createDocument;
import static br.ufsc.monitoraconapir.util.pdf.PdfUtils.write;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraconapir.enumeration.TipoOpcao;
import br.ufsc.monitoraconapir.model.Objetivo;
import br.ufsc.monitoraconapir.model.Ocorrencia;
import br.ufsc.monitoraconapir.model.Opcao;
import br.ufsc.monitoraconapir.model.Ppa;
import br.ufsc.monitoraconapir.model.Programa;
import br.ufsc.monitoraconapir.model.Proposta;
import br.ufsc.monitoraconapir.model.TermoBusca;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.repository.PropostaRepository;
import br.ufsc.monitoraconapir.repository.TermoBuscaRepository;
import br.ufsc.monitoraconapir.service.PropostaService;
import br.ufsc.monitoraconapir.util.pdf.FiltroPesquisa;
import br.ufsc.monitoraconapir.util.pdf.Subtema;
import br.ufsc.monitoraconapir.util.pdf.TabelaPdfProposta;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ListItem;

@Service
public class PropostaServiceImpl implements PropostaService {

	@Autowired
	private PropostaRepository repository;

	@Autowired
	private OpcaoRepository opcaoRepository;

	@Autowired
	private TermoBuscaRepository termoBuscaRepository;

	@Override
	public List<Proposta> buscar(final boolean prioritarias, final String filtro) {
		if(!prioritarias && filtro == null)
			return repository.findAllResolutionsWithOccurrences();

		if(prioritarias)
			return repository.findPriorityWithOccurrences();

		final TermoBusca termoBusca = termoBuscaRepository.findByValue(filtro);
		if(termoBusca != null)
			termoBuscaRepository.incrementSearch(termoBusca.getId());
		return repository.findWithTextWithOccurrences(filtro);
	}

	private List<Proposta> busca(final FiltroPesquisa filtroPesquisa, final Subtema subtema, final String filtroTexto) {
		if(StringUtils.isBlank(filtroTexto))
			switch(filtroPesquisa) {
				case TODAS:
					if(Subtema.TODOS.equals(subtema))
						return repository.findAllResolutions();
					return repository.findAllResolutionsFromSubtheme(subtema);

				case PRIORITARIAS:
					if(Subtema.TODOS.equals(subtema))
						return repository.findPriority();
					return repository.findPriorityFromSubtheme(subtema);

			}
		else
			switch(filtroPesquisa) {
				case TODAS:
					if(Subtema.TODOS.equals(subtema))
						return repository.findWithText(filtroTexto);
					return repository.findWithTextFromSubtheme(filtroTexto, subtema);

				case PRIORITARIAS:
					if(Subtema.TODOS.equals(subtema))
						return repository.findPriorityWithText(filtroTexto);
					return repository.findPriorityWithTextFromSubtheme(filtroTexto, subtema);
			}
		return Collections.emptyList();
	}

	@Override
	public File gerarPdf(final FiltroPesquisa filtroPesquisa, final Subtema subtema, final String filtroTexto) throws IOException,
		DocumentException {
		final File file = File.createTempFile("Resoluções", ".pdf");
		final Document pdf = createDocument(file);

		pdf.open();

		final List<Proposta> propostas = busca(filtroPesquisa, subtema, filtroTexto);

		if(Subtema.TODOS.equals(subtema)) {
			final List<Proposta> propostasSubtema1 = new ArrayList<>();
			final List<Proposta> propostasSubtema2 = new ArrayList<>();
			final List<Proposta> propostasSubtema3 = new ArrayList<>();
			final List<Proposta> propostasSubtema4 = new ArrayList<>();

			for(final Proposta proposta : propostas)
				switch(proposta.getSubtema()) {
					case SUBTEMA_1:
						propostasSubtema1.add(proposta);
						break;

					case SUBTEMA_2:
						propostasSubtema2.add(proposta);
						break;

					case SUBTEMA_3:
						propostasSubtema3.add(proposta);
						break;

					case SUBTEMA_4:
						propostasSubtema4.add(proposta);
						break;

					default:
						break;
				}

			if(propostasSubtema1.size() > 0) {
				final TabelaPdfProposta tabela1 = new TabelaPdfProposta(Subtema.SUBTEMA_1, propostasSubtema1);
				adicionaDadosFiltro(pdf, filtroPesquisa, subtema, filtroTexto);
				pdf.add(tabela1);
				pdf.newPage();
			}

			if(propostasSubtema2.size() > 0) {
				final TabelaPdfProposta tabela2 = new TabelaPdfProposta(Subtema.SUBTEMA_2, propostasSubtema2);
				adicionaDadosFiltro(pdf, filtroPesquisa, subtema, filtroTexto);
				pdf.add(tabela2);
				pdf.newPage();
			}

			if(propostasSubtema3.size() > 0) {
				final TabelaPdfProposta tabela3 = new TabelaPdfProposta(Subtema.SUBTEMA_3, propostasSubtema3);
				adicionaDadosFiltro(pdf, filtroPesquisa, subtema, filtroTexto);
				pdf.add(tabela3);
				pdf.newPage();
			}

			if(propostasSubtema4.size() > 0) {
				final TabelaPdfProposta tabela4 = new TabelaPdfProposta(Subtema.SUBTEMA_4, propostasSubtema4);
				adicionaDadosFiltro(pdf, filtroPesquisa, subtema, filtroTexto);
				pdf.add(tabela4);
				pdf.newPage();
			}
		} else {
			final TabelaPdfProposta tabela = new TabelaPdfProposta(subtema, propostas);
			adicionaDadosFiltro(pdf, filtroPesquisa, subtema, filtroTexto);
			pdf.add(tabela);
			pdf.newPage();
		}

		pdf.close();

		return file;
	}

	private void adicionaDadosFiltro(final Document document, final FiltroPesquisa filtro, final Subtema subtema, final String filtroTexto)
		throws DocumentException {
		document.add(write("Lista de Resoluções", FONTE_NEGRITO));
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(write("Filtro: ", FONTE_NEGRITO));
		document.add(write(filtro.getDescricao()));

		if(StringUtils.isNotBlank(filtroTexto)) {
			document.add(write(" - Contendo o termo de busca "));
			document.add(write(filtroTexto, FONTE_NEGRITO));
		}

		document.add(Chunk.NEWLINE);
		document.add(write("Subtema: ", FONTE_NEGRITO));
		document.add(write(subtema.getResumo()));
	}

	@Override
	public File gerarPdf(final Integer idProposta) throws IOException, DocumentException {
		final Proposta p = repository.findDetails(idProposta);
		final File file = File.createTempFile(String.format("Resolução_%s", p.getIdentificador()), ".pdf");
		final Document pdf = createDocument(file);

		pdf.open();

		pdf.add(write(String.format("Resolução %s", p.getIdentificador()), FONTE_TITULO));
		pdf.add(Chunk.NEWLINE);
		pdf.add(Chunk.NEWLINE);
		pdf.add(write("Subtema: ", FONTE_NEGRITO));
		pdf.add(write(p.getSubtema().getResumo()));
		pdf.add(Chunk.NEWLINE);
		pdf.add(write("Prioritária: ", FONTE_NEGRITO));
		pdf.add(write(p.isPrioritaria() ? "SIM" : "NÃO"));
		pdf.add(Chunk.NEWLINE);
		pdf.add(Chunk.NEWLINE);
		pdf.add(write(p.getProposta()));

		for(final Ocorrencia o : p.getOcorrencias()) {
			pdf.newPage();

			if(CollectionUtils.isNotEmpty(o.getIdOpcoes())) {
				final List<Opcao> opcoes = opcaoRepository.findAll(o.getIdOpcoes());
				final Ppa ppa = opcoes.get(0).getPpa();

				pdf.add(write(String.format("PPA %s", ppa.getDescricao()), FONTE_TITULO));

				pdf.add(Chunk.NEWLINE);
				pdf.add(Chunk.NEWLINE);
				if(o.getVinculoPpa().isPossui() && !Boolean.FALSE.equals(o.getVinculoPpa().isExibir())) {
					pdf.add(write("Vínculo da Resolução com o PPA", FONTE_NEGRITO));
					pdf.add(Chunk.NEWLINE);
					pdf.add(Chunk.NEWLINE);
					pdf.add(escreveListaMetas(opcoes));
					pdf.add(Chunk.NEWLINE);
				}

				pdf.add(Chunk.NEWLINE);
				pdf.add(Chunk.NEWLINE);
				if(o.getVinculoOutros().isPossui() && !Boolean.FALSE.equals(o.getVinculoOutros().isExibir())
						&& Boolean.TRUE.equals(o.getVinculoOutros().isExibirOutros()) && StringUtils.isNotBlank(o.getVinculoOutros().getOutros())) {
					pdf.add(write("Vínculo com outras Políticas, Programas ou Planos. Quais?", FONTE_NEGRITO));
					pdf.add(Chunk.NEWLINE);
					pdf.add(Chunk.NEWLINE);
					pdf.add(write(o.getVinculoOutros().getOutros()));
					pdf.add(Chunk.NEWLINE);
				}

				pdf.add(Chunk.NEWLINE);
				pdf.add(Chunk.NEWLINE);
				if(!Boolean.FALSE.equals(o.getAcoes().isExibir()) && StringUtils.isNotBlank(o.getAcoes().getDescricao())) {
					pdf.add(write("Ações governamentais em execução relacionadas a esta Resolução", FONTE_NEGRITO));
					pdf.add(Chunk.NEWLINE);
					pdf.add(Chunk.NEWLINE);
					pdf.add(write(o.getAcoes().getDescricao()));
					pdf.add(Chunk.NEWLINE);
				}

				pdf.add(Chunk.NEWLINE);
				pdf.add(Chunk.NEWLINE);
				if(!Boolean.FALSE.equals(o.getQuestoes().isExibir()) && StringUtils.isNotBlank(o.getQuestoes().getDescricao())) {
					pdf.add(write("Novas questões que esta Resolução traz para o planejamento governamental", FONTE_NEGRITO));
					pdf.add(Chunk.NEWLINE);
					pdf.add(Chunk.NEWLINE);
					pdf.add(write(o.getQuestoes().getDescricao()));
					pdf.add(Chunk.NEWLINE);
				}

				pdf.add(Chunk.NEWLINE);
				pdf.add(Chunk.NEWLINE);
				if(!Boolean.FALSE.equals(o.getFontes().isExibir()) && StringUtils.isNotBlank(o.getFontes().getDescricao())) {
					pdf.add(write("Fontes recomendadas de informações sobre o tema", FONTE_NEGRITO));
					pdf.add(Chunk.NEWLINE);
					pdf.add(Chunk.NEWLINE);
					pdf.add(write(o.getFontes().getDescricao()));
					pdf.add(Chunk.NEWLINE);
				}
			}
		}

		pdf.close();

		return file;
	}

	private com.itextpdf.text.List escreveListaMetas(final List<Opcao> opcoes) {
		final Map<Integer, ProgramaPdf> programas = new TreeMap<>();

		for(final Opcao opcao : opcoes) {
			final ProgramaPdf programaPdf;
			final Programa programa = opcao.getPrograma();
			if(programas.containsKey(programa.getId())) {
				programaPdf = programas.get(programa.getId());
			} else {
				programaPdf = new ProgramaPdf(programa.getDescricao());
				programas.put(programa.getId(), programaPdf);
			}

			final Map<Integer, ObjetivoPdf> objetivos = programaPdf.getObjetivos();
			final ObjetivoPdf objetivoPdf;
			final Objetivo objetivo = opcao.getObjetivo();
			if(objetivos.containsKey(objetivo.getId())) {
				objetivoPdf = objetivos.get(objetivo.getId());
			} else {
				objetivoPdf = new ObjetivoPdf(objetivo.getDescricao(), opcao.getOrgao());
				objetivos.put(objetivo.getId(), objetivoPdf);
			}

			final Map<Integer, MetaIniciativaPdf> metasIniciativas = objetivoPdf.getMetasIniciativas();
			final MetaIniciativaPdf metaIniciativaPdf;
			if(metasIniciativas.containsKey(opcao.getId())) {
				metaIniciativaPdf = metasIniciativas.get(opcao.getId());
			} else {
				metaIniciativaPdf = new MetaIniciativaPdf(opcao.getTipo(), opcao.getMetaIniciativa());
				metasIniciativas.put(opcao.getId(), metaIniciativaPdf);
			}
		}

		final com.itextpdf.text.List listaProgramas = new com.itextpdf.text.List();
		for(final ProgramaPdf programa : programas.values()) {
			listaProgramas.add(new ListItem(write(String.format("Programa: ".concat(programa.getDescricao())))));
			final com.itextpdf.text.List listaObjetivos = new com.itextpdf.text.List(20);
			for(final ObjetivoPdf objetivo : programa.getObjetivos().values()) {
				listaObjetivos.add(new ListItem(write("Objetivo: ".concat(objetivo.getDescricao()))));
				listaObjetivos.add(new ListItem(write("Órgão: ".concat(objetivo.getOrgao()))));
				final com.itextpdf.text.List listaMetasIniciativas = new com.itextpdf.text.List(20);
				for(final MetaIniciativaPdf metaIniciativa : objetivo.getMetasIniciativas().values()) {
					listaMetasIniciativas.add(new ListItem(write(metaIniciativa.getTipo().getDescricao().concat(": ").concat(metaIniciativa.getDescricao()))));
				}
				listaObjetivos.add(listaMetasIniciativas);
			}
			listaProgramas.add(listaObjetivos);
		}

		return listaProgramas;
	}

	private static final class ProgramaPdf {

		private final String descricao;
		private final Map<Integer, ObjetivoPdf> objetivos;

		public ProgramaPdf(final String descricao) {
			this.descricao = descricao;
			objetivos = new TreeMap<>();
		}

		public String getDescricao() {
			return descricao;
		}

		public Map<Integer, ObjetivoPdf> getObjetivos() {
			return objetivos;
		}

	}

	private static final class ObjetivoPdf {

		private final String descricao;
		private final String orgao;
		private final Map<Integer, MetaIniciativaPdf> metasIniciativas;

		public ObjetivoPdf(final String descricao, final String orgao) {
			this.descricao = descricao;
			this.orgao = orgao;
			metasIniciativas = new TreeMap<>();
		}

		public String getDescricao() {
			return descricao;
		}

		public String getOrgao() {
			return orgao;
		}

		public Map<Integer, MetaIniciativaPdf> getMetasIniciativas() {
			return metasIniciativas;
		}

	}

	private static final class MetaIniciativaPdf {

		private final TipoOpcao tipo;
		private final String descricao;

		public MetaIniciativaPdf(final TipoOpcao tipo, final String descricao) {
			this.tipo = tipo;
			this.descricao = descricao;
		}

		public TipoOpcao getTipo() {
			return tipo;
		}

		public String getDescricao() {
			return descricao;
		}

	}

}
