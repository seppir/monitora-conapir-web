package br.ufsc.monitoraconapir.service.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraconapir.dto.AssuntoDTO;
import br.ufsc.monitoraconapir.dto.PoliticaDTO;
import br.ufsc.monitoraconapir.model.Assunto;
import br.ufsc.monitoraconapir.model.Politica;
import br.ufsc.monitoraconapir.model.Ppa;
import br.ufsc.monitoraconapir.model.Programa;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.service.ProgramaService;

@Service
public class ProgramaServiceImpl implements ProgramaService {

	@Autowired
	private OpcaoRepository repository;

	@Override
	public Map<String, Map<Integer, PoliticaDTO>> findAll() {
		final List<Ppa> ppas = repository.findPpas();
		final Map<String, Map<Integer, PoliticaDTO>> mapPpas = new TreeMap<>();

		for(final Ppa ppa : ppas) {
			final List<Politica> politicas = repository.findPoliciesByPpa(ppa.getId());
			final Map<Integer, PoliticaDTO> mapPoliticas = new TreeMap<>();

			for(final Politica politica : politicas) {
				final List<Assunto> assuntos = repository.findSubjectsByPolicy(politica.getId());
				final Map<Integer, AssuntoDTO> mapAssuntos = new TreeMap<>();

				for(final Assunto assunto : assuntos) {
					final List<Programa> programas = repository.findProgramsBySubject(assunto.getId());
					mapAssuntos.put(assunto.getId(), new AssuntoDTO(assunto, programas));
				}
				mapPoliticas.put(politica.getId(), new PoliticaDTO(politica, mapAssuntos));
			}
			mapPpas.put(ppa.getDescricao(), mapPoliticas);
		}

		return mapPpas;
	}

	@Override
	public Map<Integer, Integer> countResolutions() {
		final List<Programa> programas = repository.findPrograms();
		final Map<Integer, Integer> contador = new TreeMap<>();
		for(final Programa programa : programas) {
			contador.put(programa.getId(), repository.countProgramResolutions(programa.getId()));
		}
		return contador;
	}

}
