package br.ufsc.monitoraconapir.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.ufsc.monitoraconapir.model.Proposta;
import br.ufsc.monitoraconapir.util.pdf.FiltroPesquisa;
import br.ufsc.monitoraconapir.util.pdf.Subtema;

import com.itextpdf.text.DocumentException;

public interface PropostaService {

	List<Proposta> buscar(final boolean prioritarias, final String filtro);

	File gerarPdf(final Integer idProposta) throws IOException, DocumentException;

	File gerarPdf(final FiltroPesquisa filtroPesquisa, final Subtema subtema, final String filtroTexto) throws IOException, DocumentException;

}
