package br.ufsc.monitoraconapir.service;

import java.util.Map;

import br.ufsc.monitoraconapir.dto.PoliticaDTO;

public interface ProgramaService {

	Map<String, Map<Integer, PoliticaDTO>> findAll();

	Map<Integer, Integer> countResolutions();

}
