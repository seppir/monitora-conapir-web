package br.ufsc.monitoraconapir.dto;

import java.util.Map;

import br.ufsc.monitoraconapir.model.Politica;

public class PoliticaDTO {

	private Politica politica;

	private Map<Integer, AssuntoDTO> assuntos;

	public PoliticaDTO(final Politica politica, final Map<Integer, AssuntoDTO> assuntos) {
		this.politica = politica;
		this.assuntos = assuntos;
	}

	public Politica getPolitica() {
		return politica;
	}

	public void setPolitica(final Politica politica) {
		this.politica = politica;
	}

	public Map<Integer, AssuntoDTO> getAssuntos() {
		return assuntos;
	}

	public void setAssuntos(final Map<Integer, AssuntoDTO> assuntos) {
		this.assuntos = assuntos;
	}

}
