package br.ufsc.monitoraconapir.dto;

import java.util.List;

import br.ufsc.monitoraconapir.model.Assunto;
import br.ufsc.monitoraconapir.model.Programa;

public class AssuntoDTO {

	private Assunto assunto;

	private List<Programa> programas;

	public AssuntoDTO(final Assunto assunto, final List<Programa> programas) {
		this.assunto = assunto;
		this.programas = programas;
	}

	public Assunto getAssunto() {
		return assunto;
	}

	public void setAssunto(final Assunto assunto) {
		this.assunto = assunto;
	}

	public List<Programa> getProgramas() {
		return programas;
	}

	public void setProgramas(final List<Programa> programas) {
		this.programas = programas;
	}

}
