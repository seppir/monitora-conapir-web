package br.ufsc.monitoraconapir.rss;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.feed.AbstractRssFeedView;

import br.ufsc.monitoraconapir.config.WebAppInitializer;
import br.ufsc.monitoraconapir.model.Proposta;

import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Content;
import com.rometools.rome.feed.rss.Item;

public class RssFeedView extends AbstractRssFeedView {

	@Override
	protected void buildFeedMetadata(final Map<String, Object> model, final Channel feed, final HttpServletRequest request) {
		feed.setTitle("Monitora CONAPIR");
		feed.setLink(WebAppInitializer.getUrlBase().concat("feedrss"));
		feed.setDescription("Últimas alterações");
		super.buildFeedMetadata(model, feed, request);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected List<Item> buildFeedItems(final Map<String, Object> model, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final List<Proposta> propostas = (List<Proposta>) model.get("rssContent");
		final String detalhesUrl = WebAppInitializer.getUrlBase().concat("page/detalhes/#/?id=%d");

		final List<Item> items = new ArrayList<>(propostas.size());

		for(final Proposta proposta : propostas) {
			final Item item = new Item();
			item.setTitle(proposta.getIdentificador());
			item.setLink(String.format(detalhesUrl, proposta.getId()));
			item.setPubDate(proposta.getDataCadastro());

			final Content content = new Content();
			content.setValue(proposta.getProposta());

			item.setContent(content);

			items.add(item);
		}

		return items;
	}
}
