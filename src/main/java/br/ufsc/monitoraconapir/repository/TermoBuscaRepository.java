package br.ufsc.monitoraconapir.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraconapir.model.TermoBusca;

@Repository
public interface TermoBuscaRepository extends JpaRepository<TermoBusca, Integer> {

	@Modifying
	@Transactional
	@Query("update TermoBusca t set t.buscas = t.buscas + 1 where t.id = ?")
	void incrementSearch(final Integer id);

	@Override
	@Query("from TermoBusca t order by t.buscas desc")
	List<TermoBusca> findAll();

	@Query("from TermoBusca t where t.valor = ?")
	TermoBusca findByValue(final String value);

}
