package br.ufsc.monitoraconapir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraconapir.model.Anexo;

@Repository
public interface AnexoRepository extends JpaRepository<Anexo, Integer> {

	@Query(value = "SELECT a.* FROM ocorrencia_arquivo a WHERE a.id_ocorrencia = ? AND a.arquivo NOT LIKE ''", nativeQuery = true)
	List<Anexo> findByOccurrence(final Integer occurrenceId);

}
