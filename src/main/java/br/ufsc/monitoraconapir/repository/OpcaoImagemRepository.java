package br.ufsc.monitoraconapir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraconapir.model.OpcaoImagem;

@Repository
public interface OpcaoImagemRepository extends JpaRepository<OpcaoImagem, Integer> {

	@Query("from OpcaoImagem o order by o.id")
	List<OpcaoImagem> findAllOrderById();

}
