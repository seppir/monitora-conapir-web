package br.ufsc.monitoraconapir.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraconapir.model.Proposta;
import br.ufsc.monitoraconapir.util.pdf.Subtema;

@Repository
public interface PropostaRepository extends JpaRepository<Proposta, Integer> {

	@Modifying
	@Transactional
	@Query("update Proposta p set p.acessos = p.acessos + 1 where p.id = ?")
	void incrementAccess(final Integer id);

	@Query("from Proposta p "
		+ "join fetch p.ocorrencias "
		+ "where p.id = ?")
	Proposta findDetails(final Integer id);

	@Query("select distinct p from Proposta p "
		+ "where p.nivel = 12 order by p.id, p.prioridade")
	List<Proposta> findAllResolutions();

	@Query("select distinct p from Proposta p "
		+ "join fetch p.ocorrencias "
		+ "where p.nivel = 12 order by p.id, p.prioridade")
	List<Proposta> findAllResolutionsWithOccurrences();

	@Query("from Proposta p "
		+ "where p.nivel = 12 and p.prioridade <= 10 order by p.id, p.prioridade")
	List<Proposta> findPriority();

	@Query("select distinct p from Proposta p "
		+ "join fetch p.ocorrencias "
		+ "where p.nivel = 12 and p.prioridade <= 10 order by p.id, p.prioridade")
	List<Proposta> findPriorityWithOccurrences();

	@Query("from Proposta p "
		+ "where p.nivel = 12 and (lower(p.proposta) like lower(concat('%', :text, '%'))) or (p.id like concat('%', :text, '%'))")
	List<Proposta> findWithText(@Param("text") final String text);

	@Query("select distinct p from Proposta p "
		+ "join fetch p.ocorrencias "
		+ "where p.nivel = 12 and (lower(p.proposta) like lower(concat('%', :text, '%'))) or (p.id like concat('%', :text, '%'))")
	List<Proposta> findWithTextWithOccurrences(@Param("text") final String text);

	@Query("from Proposta p "
		+ "where p.nivel = 12 and p.subtema = ? order by p.id, p.prioridade")
	List<Proposta> findAllResolutionsFromSubtheme(final Subtema subtema);

	@Query("from Proposta p "
		+ "where p.nivel = 12 and p.prioridade <= 10 and p.subtema = ? order by p.id, p.prioridade")
	List<Proposta> findPriorityFromSubtheme(final Subtema subtema);

	@Query("from Proposta p "
		+ "where p.nivel = 12 and (lower(p.proposta) like lower(concat('%', :text, '%')) or (p.id like concat('%', :text, '%'))) and p.subtema = :subtema")
	List<Proposta> findWithTextFromSubtheme(@Param("text") final String filtroTexto, @Param("subtema") final Subtema subtema);

	@Query("from Proposta p "
		+ "where p.nivel = 12 and (lower(p.proposta) like lower(concat('%', :text, '%')) or (p.id like concat('%', :text, '%'))) and p.prioridade <= 10 order by p.id, p.prioridade")
	List<Proposta> findPriorityWithText(@Param("text") final String filtroTexto);

	@Query("from Proposta p "
		+ "where p.nivel = 12 and (lower(p.proposta) like lower(concat('%', :text, '%')) or (p.id like concat('%', :text, '%'))) and p.subtema = :subtema and p.prioridade <= 10 order by p.id, p.prioridade")
	List<Proposta> findPriorityWithTextFromSubtheme(@Param("text") final String filtroTexto, @Param("subtema") final Subtema subtema);

	@Query(value = "SELECT DISTINCT p.* "
		+ "FROM opcoes op "
		+ "JOIN ocorrencia oc ON LOCATE(CONCAT('_', ?, '_'), CONCAT('_', REPLACE(oc.id_meta, ',', '_'), '_')) "
		+ "JOIN proposta p ON p.id = oc.proposta", nativeQuery = true)
	List<Proposta> findByOption(final Integer idOpcao);

	@Query("from Proposta p where p.nivel = 12 order by p.dataCadastro, p.id desc")
	List<Proposta> findLastChanges();

	@Query(value = "SELECT p.* FROM proposta p WHERE p.nivel = 12 ORDER BY p.data_cadastro, p.id DESC LIMIT 10", nativeQuery = true)
	List<Proposta> findLastChangesShorten();

}
