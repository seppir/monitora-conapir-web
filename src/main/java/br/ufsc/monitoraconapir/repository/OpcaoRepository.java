package br.ufsc.monitoraconapir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.ufsc.monitoraconapir.model.Assunto;
import br.ufsc.monitoraconapir.model.Objetivo;
import br.ufsc.monitoraconapir.model.Opcao;
import br.ufsc.monitoraconapir.model.Politica;
import br.ufsc.monitoraconapir.model.Ppa;
import br.ufsc.monitoraconapir.model.Programa;

public interface OpcaoRepository extends JpaRepository<Opcao, Integer> {

	@Query("select distinct o.programa from Opcao o where o.programa.id = ?")
	Programa findProgram(final Integer idPrograma);

	@Query("select distinct o.ppa from Opcao o "
		+ "order by o.ppa.id")
	List<Ppa> findPpas();

	@Query("select distinct o.politica from Opcao o "
		+ "where o.politica.id != 0 and trim(o.politica.descricao) != '' "
		+ "order by o.politica.id")
	List<Politica> findPolicies();

	@Query("select distinct o.politica from Opcao o "
		+ "where o.ppa.id = ? "
		+ "and o.politica.id != 0 and trim(o.politica.descricao) != '' "
		+ "order by o.politica.descricao")
	List<Politica> findPoliciesByPpa(final Integer idPpa);

	@Query("select distinct o.assunto from Opcao o "
		+ "where o.politica.id = ? "
		+ "and o.assunto.id != 0 and trim(o.assunto.descricao) != '' "
		+ "order by o.assunto.descricao")
	List<Assunto> findSubjectsByPolicy(final Integer idPolitica);

	@Query("select distinct o.programa from Opcao o "
		+ "where o.programa.id != 0 and trim(o.programa.descricao) != '' "
		+ "order by o.programa.id")
	List<Programa> findPrograms();

	@Query("select distinct o.programa from Opcao o "
		+ "where o.assunto.id = ? "
		+ "and o.programa.id != 0 and trim(o.programa.descricao) != '' "
		+ "order by o.programa.descricao")
	List<Programa> findProgramsBySubject(final Integer idAssunto);

	@Query("select distinct o.objetivo from Opcao o "
		+ "where o.objetivo.id in :ids "
		+ "order by o.objetivo.id")
	List<Objetivo> findObjectives(@Param("ids") final List<Integer> ids);

	@Query("select distinct o.objetivo from Opcao o where o.programa.id = ?")
	List<Objetivo> findObjectivesByProgram(final Integer idPrograma);

	@Query("from Opcao o where o.objetivo.id = ?")
	List<Opcao> findByObjective(final Integer idObjetivo);

	@Query(value = "SELECT COUNT(oc.id) FROM ocorrencia oc, opcoes op "
		+ "WHERE LOCATE(CONCAT('_', op.id, '_'), CONCAT('_', REPLACE(oc.id_meta, ',', '_'), '_')) AND op.id_programa = ? "
		+ "AND id_politica <> 0 AND TRIM(politica) <> '' AND id_assunto <> 0 AND assunto <> ''", nativeQuery = true)
	Integer countProgramResolutions(final Integer idPrograma);

	@Query(value = "SELECT COUNT(oc.id) FROM ocorrencia oc, opcoes op "
		+ "WHERE LOCATE(CONCAT('_', op.id, '_'), CONCAT('_', REPLACE(oc.id_meta, ',', '_'), '_')) AND op.id_objetivo = ? "
		+ "AND id_politica <> 0 AND TRIM(politica) <> '' AND id_assunto <> 0 AND assunto <> ''", nativeQuery = true)
	Integer countObjectiveResolutions(final Integer idObjetivo);

	@Query(value = "SELECT COUNT(oc.id) FROM ocorrencia oc "
		+ "WHERE LOCATE(CONCAT('_', ?, '_'), CONCAT('_', REPLACE(oc.id_meta, ',', '_'), '_'))", nativeQuery = true)
	Integer countOptionResolutions(final Integer idOpcao);

}
