package br.ufsc.monitoraconapir.endpoint;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.Proposta;
import br.ufsc.monitoraconapir.repository.PropostaRepository;
import br.ufsc.monitoraconapir.service.PropostaService;
import br.ufsc.monitoraconapir.util.pdf.FiltroPesquisa;
import br.ufsc.monitoraconapir.util.pdf.Subtema;

import com.itextpdf.text.DocumentException;

@RestController
@RequestMapping(value = "/proposta")
public class PropostaController {

	@Autowired
	private PropostaRepository repository;

	@Autowired
	private PropostaService service;

	@RequestMapping("/{id}")
	public Proposta getDetails(@PathVariable final Integer id) {
		repository.incrementAccess(id);
		return repository.findDetails(id);
	}

	@RequestMapping(value = "/buscar", method = RequestMethod.POST)
	public List<Proposta> search(@RequestParam(required = false) final boolean prioritarias, @RequestParam(required = false) final String filtro) {
		return service.buscar(prioritarias, filtro);
	}

	@RequestMapping(value = "/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public FileSystemResource pdf(final FiltroPesquisa filtro, final Integer subtema, @RequestParam(required = false) final String texto,
		final HttpServletResponse response) throws IOException, DocumentException {
		final File pdf = service.gerarPdf(filtro, Subtema.fromId(subtema), texto);
		response.setHeader("Content-Disposition", String.format("attachment;filename=%s", pdf.getName()));
		return new FileSystemResource(pdf);
	}

	@RequestMapping(value = "/{id}/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public FileSystemResource pdf(@PathVariable final Integer id, final HttpServletResponse response) throws IOException, DocumentException {
		final File pdf = service.gerarPdf(id);
		response.setHeader("Content-Disposition", String.format("attachment;filename=%s", pdf.getName()));
		return new FileSystemResource(pdf);
	}

	@RequestMapping(value = "/poropcao/{idOpcao}", method = RequestMethod.GET)
	public List<Proposta> findByOption(@PathVariable final Integer idOpcao) {
		return repository.findByOption(idOpcao);
	}

	@RequestMapping(value = "/ultimasalteracoes", method = RequestMethod.GET)
	public List<Proposta> lastChanges(final boolean resumido) {
		if(resumido)
			return repository.findLastChangesShorten();
		return repository.findLastChanges();
	}

}
