package br.ufsc.monitoraconapir.endpoint;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.Anexo;
import br.ufsc.monitoraconapir.repository.AnexoRepository;

@RestController
@RequestMapping("/anexo")
public class AnexoController {

	@Autowired
	private AnexoRepository repository;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public Resource downloadAnexo(@PathVariable final Integer id, final HttpServletResponse response) throws IOException {
		final Anexo anexo = repository.findOne(id);

		response.setHeader("Content-Disposition", "attachment;filename=".concat(anexo.getNome()));

		final File file = anexo.toFile();
		return new FileSystemResource(file);
	}

	@RequestMapping(value = "/ocorrencia/{idOcorrencia}", method = RequestMethod.GET)
	public List<Anexo> getFromOccurrence(@PathVariable final Integer idOcorrencia) {
		return repository.findByOccurrence(idOcorrencia);
	}

}
