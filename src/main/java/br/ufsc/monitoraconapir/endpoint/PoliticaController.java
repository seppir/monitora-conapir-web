package br.ufsc.monitoraconapir.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.Politica;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;

@RestController
@RequestMapping("/politica")
public class PoliticaController {

	@Autowired
	private OpcaoRepository repository;

	@RequestMapping(value = "/todas", method = RequestMethod.GET)
	public List<Politica> findAll() {
		return repository.findPolicies();
	}

}
