package br.ufsc.monitoraconapir.endpoint;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.dto.PoliticaDTO;
import br.ufsc.monitoraconapir.model.Programa;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.service.ProgramaService;

@RestController
@RequestMapping("/programa")
public class ProgramaController {

	@Autowired
	private OpcaoRepository repository;

	@Autowired
	private ProgramaService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Programa findOne(@PathVariable final Integer id) {
		return repository.findProgram(id);
	}

	@RequestMapping(value = "/todos", method = RequestMethod.GET)
	public Map<String, Map<Integer, PoliticaDTO>> findAll() {
		return service.findAll();
	}

	@RequestMapping(value = "/contar", method = RequestMethod.GET)
	public Map<Integer, Integer> countResolutions() {
		return service.countResolutions();
	}

}
