package br.ufsc.monitoraconapir.endpoint;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.Objetivo;
import br.ufsc.monitoraconapir.model.Opcao;
import br.ufsc.monitoraconapir.repository.OpcaoRepository;
import br.ufsc.monitoraconapir.service.OpcaoService;

@RestController
@RequestMapping("/opcao")
public class OpcaoController {

	@Autowired
	private OpcaoRepository repository;

	@Autowired
	private OpcaoService service;

	@RequestMapping(value = "/todas", method = RequestMethod.POST)
	public List<Opcao> findAll(@RequestParam(value = "ids[]", required = false) final Integer[] ids) {
		if(ids != null)
			return repository.findAll(Arrays.asList(ids));
		return repository.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Opcao findOne(@PathVariable final Integer id) {
		return repository.findOne(id);
	}

	@RequestMapping(value = "/porprograma/{idPrograma}", method = RequestMethod.GET)
	public List<Objetivo> findByProgram(@PathVariable final Integer idPrograma) {
		return repository.findObjectivesByProgram(idPrograma);
	}

	@RequestMapping(value = "/porobjetivo/{idObjetivo}", method = RequestMethod.GET)
	public List<Opcao> findByObjective(@PathVariable final Integer idObjetivo) {
		return repository.findByObjective(idObjetivo);
	}

	@RequestMapping(value = "/contar", method = RequestMethod.POST)
	public Map<Integer, Integer> countResolutions(@RequestParam(value = "ids[]") final Integer[] ids) {
		return service.countResolutions(Arrays.asList(ids));
	}

}
