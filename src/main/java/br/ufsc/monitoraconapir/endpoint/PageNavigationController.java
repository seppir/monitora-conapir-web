package br.ufsc.monitoraconapir.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.ufsc.monitoraconapir.config.WebAppInitializer;

@Controller
public class PageNavigationController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(final HttpServletRequest request) {
		WebAppInitializer.setUrlBase(request.getRequestURL().toString());

		return "index";
	}

	@RequestMapping(value = "/partials/{page}", method = RequestMethod.GET)
	public String partial(@PathVariable final String page) {
		return "/partials/".concat(page);
	}

	@RequestMapping(value = "/page/{page}", method = RequestMethod.GET)
	public String page(@PathVariable final String page) {
		return page;
	}

}
