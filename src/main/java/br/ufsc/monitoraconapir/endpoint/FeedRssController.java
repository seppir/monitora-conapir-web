package br.ufsc.monitoraconapir.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.ufsc.monitoraconapir.repository.PropostaRepository;

@Controller
public class FeedRssController {

	@Autowired
	private PropostaRepository repository;

	@RequestMapping(value = "/feedrss", method = RequestMethod.GET)
	public ModelAndView feedRss() {
		final ModelAndView mav = new ModelAndView();
		mav.setViewName("rssFeedView");
		mav.addObject("rssContent", repository.findLastChanges());
		return mav;
	}

}
