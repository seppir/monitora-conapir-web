package br.ufsc.monitoraconapir.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.TermoBusca;
import br.ufsc.monitoraconapir.repository.TermoBuscaRepository;

@RestController
@RequestMapping("/termobusca")
public class TermoBuscaController {

	@Autowired
	private TermoBuscaRepository repository;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<TermoBusca> getAll() {
		return repository.findAll();
	}

}
