package br.ufsc.monitoraconapir.endpoint;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.service.ObjetivoService;

@RestController
@RequestMapping("/objetivo")
public class ObjetivoController {

	@Autowired
	private ObjetivoService service;

	@RequestMapping(value = "/contar", method = RequestMethod.POST)
	public Map<Integer, Integer> countResolutions(@RequestParam(value = "ids[]") final Integer[] ids) {
		return service.countResolutions(Arrays.asList(ids));
	}

}
