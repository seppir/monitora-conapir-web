package br.ufsc.monitoraconapir.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraconapir.model.OpcaoImagem;
import br.ufsc.monitoraconapir.repository.OpcaoImagemRepository;

@RestController
@RequestMapping("/imagem")
public class OpcaoImagemController {

	@Autowired
	private OpcaoImagemRepository repository;

	@RequestMapping("/todas")
	public List<OpcaoImagem> getAll() {
		return repository.findAllOrderById();
	}

}
