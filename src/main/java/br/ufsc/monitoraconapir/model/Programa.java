package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Programa {

	@Column(name = "id_programa")
	private Integer id;

	@Column(name = "programa")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
