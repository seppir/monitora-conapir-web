package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Ppa {

	@Column(name = "id_ppa")
	private Integer id;

	@Column(name = "ppa")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
