package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "termo_busca")
public class TermoBusca {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "valor", unique = true)
	private String valor;

	@Column(name = "buscas")
	private Integer buscas;

	public Integer getId() {
		return id;
	}

	public String getValor() {
		return valor;
	}

	public Integer getBuscas() {
		return buscas;
	}

}
