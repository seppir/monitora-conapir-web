package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Objetivo {

	@Column(name = "id_objetivo")
	private Integer id;

	@Column(name = "objetivo")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
