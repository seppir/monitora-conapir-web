package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.ufsc.monitoraconapir.enumeration.TipoOpcaoImagem;
import br.ufsc.monitoraconapir.util.converter.TipoOpcaoImagemConverter;

@Entity
@Table(name = "opcoes_arquivo")
public class OpcaoImagem {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "tipo")
	@Convert(converter = TipoOpcaoImagemConverter.class)
	private TipoOpcaoImagem tipo;

	@Column(name = "nome_arquivo")
	private String nomeArquivo;

	@Column(name = "arquivo")
	@JsonIgnore
	private byte[] arquivo;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public TipoOpcaoImagem getTipo() {
		return tipo;
	}

	public void setTipo(final TipoOpcaoImagem tipo) {
		this.tipo = tipo;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(final String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(final byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public String getArquivoBase64() {
		return new String(Base64.encodeBase64(arquivo));
	}

}
