package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;

import br.ufsc.monitoraconapir.util.converter.FlagExibirConverter;
import br.ufsc.monitoraconapir.util.converter.SimNaoConverter;

@Embeddable
public class VinculoOutros {

	@Column(name = "vinculo_outros")
	@Convert(converter = SimNaoConverter.class)
	private boolean possui;

	@Column(name = "vinculo_outros_")
	@Convert(converter = FlagExibirConverter.class)
	private Boolean exibir;

	@Column(name = "quais_outros")
	private String outros;

	@Column(name = "quais_outros_")
	@Convert(converter = FlagExibirConverter.class)
	private Boolean exibirOutros;

	public boolean isPossui() {
		return possui;
	}

	public Boolean isExibir() {
		return exibir;
	}

	public String getOutros() {
		return outros;
	}

	public Boolean isExibirOutros() {
		return exibirOutros;
	}
}
