package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;

import br.ufsc.monitoraconapir.util.converter.FlagExibirConverter;

@Embeddable
public class Fontes {

	@Column(name = "fontes")
	private String descricao;

	@Column(name = "fontes_")
	@Convert(converter = FlagExibirConverter.class)
	private Boolean exibir;

	public String getDescricao() {
		return descricao;
	}

	public Boolean isExibir() {
		return exibir;
	}

}
