package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Politica {

	@Column(name = "id_politica")
	private Integer id;

	@Column(name = "politica")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
