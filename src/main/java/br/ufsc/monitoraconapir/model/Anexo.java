package br.ufsc.monitoraconapir.model;

import java.io.File;
import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "ocorrencia_arquivo")
@Entity
@Where(clause = "ativo = 'S'")
public class Anexo {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "id_ocorrencia")
	@JsonIgnore
	private Integer idOcorrencia;

	@Column(name = "nome_arquivo")
	private String nome;

	@Column(name = "arquivo")
	private byte[] conteudo;

	public File toFile() throws IOException {
		final String[] partesNome = StringUtils.split(getNome(), ".");
		final String[] nome = new String[partesNome.length - 1];

		for(int i = 0; i < nome.length; i++)
			nome[i] = partesNome[i];

		final String nomeArquivo = StringUtils.join(nome, ".");
		final String extensao = ".".concat(partesNome[partesNome.length - 1]);
		final File file = File.createTempFile(nomeArquivo, extensao);
		FileUtils.writeByteArrayToFile(file, getConteudo());

		return file;
	}

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public Integer getIdOcorrencia() {
		return idOcorrencia;
	}

	public void setIdOcorrencia(final Integer idOcorrencia) {
		this.idOcorrencia = idOcorrencia;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public byte[] getConteudo() {
		return conteudo;
	}

	public void setConteudo(final byte[] conteudo) {
		this.conteudo = conteudo;
	}

}
