package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;

import br.ufsc.monitoraconapir.util.converter.FlagExibirConverter;
import br.ufsc.monitoraconapir.util.converter.SimNaoConverter;

@Embeddable
public class VinculoPpa {

	@Column(name = "vinculo_ppa")
	@Convert(converter = SimNaoConverter.class)
	private boolean possui;

	@Column(name = "vinculo_ppa_")
	@Convert(converter = FlagExibirConverter.class)
	private Boolean exibir;

	public boolean isPossui() {
		return possui;
	}

	public Boolean isExibir() {
		return exibir;
	}

}
