package br.ufsc.monitoraconapir.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.ufsc.monitoraconapir.enumeration.TipoOpcao;
import br.ufsc.monitoraconapir.util.converter.TipoOpcaoConverter;

@Table(name = "opcoes")
@Entity
public class Opcao {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id_ppa")),
		@AttributeOverride(name = "descricao", column = @Column(name = "ppa"))
	})
	private Ppa ppa;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id_politica")),
		@AttributeOverride(name = "descricao", column = @Column(name = "politica"))
	})
	private Politica politica;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id_assunto")),
		@AttributeOverride(name = "descricao", column = @Column(name = "assunto"))
	})
	private Assunto assunto;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id_programa")),
		@AttributeOverride(name = "descricao", column = @Column(name = "programa"))
	})
	private Programa programa;

	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id_objetivo")),
		@AttributeOverride(name = "descricao", column = @Column(name = "objetivo"))
	})
	private Objetivo objetivo;

	@Column(name = "orgao")
	private String orgao;

	@Column(name = "meta_iniciativa")
	private String metaIniciativa;

	@Column(name = "tipo")
	@Convert(converter = TipoOpcaoConverter.class)
	private TipoOpcao tipo;

	public Integer getId() {
		return id;
	}

	public Ppa getPpa() {
		return ppa;
	}

	public Politica getPolitica() {
		return politica;
	}

	public Assunto getAssunto() {
		return assunto;
	}

	public Programa getPrograma() {
		return programa;
	}

	public Objetivo getObjetivo() {
		return objetivo;
	}

	public String getOrgao() {
		return orgao;
	}

	public String getMetaIniciativa() {
		return metaIniciativa;
	}

	public TipoOpcao getTipo() {
		return tipo;
	}

}
