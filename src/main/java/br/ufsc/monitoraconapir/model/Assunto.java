package br.ufsc.monitoraconapir.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Assunto {

	@Column(name = "id_assunto")
	private Integer id;

	@Column(name = "assunto")
	private String descricao;

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

}
