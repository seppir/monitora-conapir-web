package br.ufsc.monitoraconapir.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.ufsc.monitoraconapir.util.converter.SimNaoConverter;
import br.ufsc.monitoraconapir.util.pdf.Subtema;

import com.fasterxml.jackson.annotation.JsonGetter;

@Table(name = "proposta")
@Entity
@Where(clause = "ativo = 'S'")
public class Proposta {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "id_conferencia")
	private Integer idConferencia;

	@Column(name = "id_eixo")
	@Enumerated(EnumType.ORDINAL)
	private Subtema subtema;

	@Column(name = "proposta")
	private String proposta;

	@Column(name = "proposta_original")
	private String propostaOriginal;

	@Column(name = "palavra_chave")
	private String palavraChave;

	@Column(name = "data_cadastro")
	private Date dataCadastro;

	@Column(name = "ativo")
	@Convert(converter = SimNaoConverter.class)
	private boolean ativo;

	@Column(name = "nivel")
	private Integer nivel;

	@Column(name = "justificativa")
	private String justificativa;

	@Column(name = "impertinencia")
	private Integer impertinencia;

	@Column(name = "identificador")
	private String identificador;

	@Column(name = "prioridade")
	private Integer prioridade;

	@Column(name = "acessos")
	private Integer acessos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proposta")
	private List<Ocorrencia> ocorrencias;

	@JsonGetter
	public boolean isPrioritaria() {
		return prioridade <= 10;
	}

	@JsonGetter
	public Integer getIdEixo() {
		if(subtema != null)
			return subtema.getId();
		return Subtema.TODOS.getId();
	}

	public Integer getId() {
		return id;
	}

	public Integer getIdConferencia() {
		return idConferencia;
	}

	public Subtema getSubtema() {
		return subtema;
	}

	public String getProposta() {
		return proposta;
	}

	public String getPropostaOriginal() {
		return propostaOriginal;
	}

	public String getPalavraChave() {
		return palavraChave;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public Integer getNivel() {
		return nivel;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public Integer getImpertinencia() {
		return impertinencia;
	}

	public String getIdentificador() {
		return identificador;
	}

	public Integer getPrioridade() {
		return prioridade;
	}

	public List<Ocorrencia> getOcorrencias() {
		return ocorrencias;
	}

	public Integer getAcessos() {
		return acessos;
	}

}
