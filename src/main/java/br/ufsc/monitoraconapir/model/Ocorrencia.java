package br.ufsc.monitoraconapir.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "ocorrencia")
@Entity
public class Ocorrencia {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@JoinColumn(name = "proposta")
	@ManyToOne(fetch = FetchType.EAGER)
	@JsonBackReference
	private Proposta proposta;

	@Embedded
	private VinculoPpa vinculoPpa;

	@Formula("(SELECT o.ppa "
		+ "FROM opcoes o "
		+ "WHERE LOCATE(CONCAT('_', o.id, '_'), CONCAT('_', REPLACE(id_meta, ',', '_'), '_')) "
		+ "LIMIT 1)")
	private String descricaoPpa;

	@Column(name = "id_meta")
	@JsonIgnore
	private String strIdOpcoes;

	@Embedded
	private VinculoOutros vinculoOutros;

	@Embedded
	private Acoes acoes;

	@Embedded
	private Questoes questoes;

	@Embedded
	private Fontes fontes;

	@Column(name = "horario")
	private Date horario;

	public Integer getId() {
		return id;
	}

	public Proposta getProposta() {
		return proposta;
	}

	@JsonGetter
	public Integer getIdProposta() {
		return proposta.getId();
	}

	public VinculoPpa getVinculoPpa() {
		return vinculoPpa;
	}

	public String getDescricaoPpa() {
		return descricaoPpa;
	}

	public String getStrIdOpcoes() {
		return strIdOpcoes;
	}

	@JsonGetter
	public List<Integer> getIdOpcoes() {
		final String[] idsArray = getStrIdOpcoes().replaceAll("_", "").split(",");
		final List<Integer> ids = new ArrayList<>();
		for(final String idStr : idsArray) {
			final Integer id = Integer.valueOf(idStr);
			if(id > 0)
				ids.add(id);
		}
		return ids;
	}

	public VinculoOutros getVinculoOutros() {
		return vinculoOutros;
	}

	public Acoes getAcoes() {
		return acoes;
	}

	public Questoes getQuestoes() {
		return questoes;
	}

	public Fontes getFontes() {
		return fontes;
	}

	public Date getHorario() {
		return horario;
	}

}
