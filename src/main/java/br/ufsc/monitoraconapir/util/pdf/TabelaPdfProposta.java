package br.ufsc.monitoraconapir.util.pdf;

import java.awt.Color;
import java.util.List;

import br.ufsc.monitoraconapir.model.Proposta;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;

public class TabelaPdfProposta extends PdfPTable {

	private static final int COLSPAN_NUMERO = 1;
	private static final int COLSPAN_DESCRICAO = 5;
	private static final int COLSPAN_SUBTEMA = COLSPAN_NUMERO + COLSPAN_DESCRICAO;

	private static final BaseColor COR_SUBTEMA = new BaseColor(Color.decode("#444444"));
	private static final BaseColor COR_CAMPOS = new BaseColor(Color.decode("#AAAAAA"));
	private static final BaseColor COR_PROPOSTA = new BaseColor(Color.decode("#EEEEEE"));

	private static final Font FONTE_SUBTEMA = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE);
	private static final Font FONTE_CAMPOS = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.RED);

	private final List<Proposta> propostas;
	private final Subtema subtema;

	public TabelaPdfProposta(final Subtema subtema, final List<Proposta> propostas) {
		super(new float[] { COLSPAN_NUMERO, COLSPAN_DESCRICAO });
		this.subtema = subtema;
		this.propostas = propostas;

		setWidthPercentage(100);
		getDefaultCell().setPadding(4);
		getDefaultCell().setPaddingBottom(6);
		getDefaultCell().setBorder(Rectangle.BOX);
		getDefaultCell().setBorderWidth(1);
		getDefaultCell().setBorderColor(BaseColor.WHITE);
		getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		getDefaultCell().setVerticalAlignment(Element.ALIGN_TOP);

		addSubtema();
		addCampos();
		setHeaderRows(2);
		addLista();
	}

	private void addSubtema() {
		getDefaultCell().setBackgroundColor(COR_SUBTEMA);
		getDefaultCell().setColspan(COLSPAN_SUBTEMA);

		addCell(PdfUtils.write(getTextoSubtema(), FONTE_SUBTEMA));
	}

	private void addCampos() {
		getDefaultCell().setBackgroundColor(COR_CAMPOS);

		getDefaultCell().setColspan(COLSPAN_NUMERO);
		addCell(PdfUtils.write("Número", FONTE_CAMPOS));

		getDefaultCell().setColspan(COLSPAN_DESCRICAO);
		addCell(PdfUtils.write("Descrição", FONTE_CAMPOS));
	}

	private void addLista() {
		getDefaultCell().setBackgroundColor(COR_PROPOSTA);

		for(final Proposta p : propostas) {
			getDefaultCell().setColspan(COLSPAN_NUMERO);
			addCell(PdfUtils.write(p.getIdentificador()));

			getDefaultCell().setColspan(COLSPAN_DESCRICAO);
			addCell(PdfUtils.write(p.getProposta()));
		}
	}

	private String getTextoSubtema() {
		String strSubtema;
		switch(subtema) {
			case SUBTEMA_1:
				strSubtema = "Estratégias para o desenvolvimento e o enfrentamento ao racismo";
				break;

			case SUBTEMA_2:
				strSubtema = "Políticas de igualdade racial no Brasil: avanços e desafios";
				break;

			case SUBTEMA_3:
				strSubtema = "Arranjos Institucionais para assegurar a sustentabilidade das políticas de igualdade racial: Sinapir, órgãos de promoção da igualdade racial, fórum de gestores, conselhos e ouvidorias";
				break;

			case SUBTEMA_4:
				strSubtema = "Participação política e controle social: igualdade racial nos espaços de decisão e mecanismos de participação da sociedade civil no monitoramento das políticas de igualdade racial";
				break;

			default:
				return "";
		}

		return String.format("Subtema %d - %s", subtema.getId(), strSubtema);
	}

	public List<Proposta> getPropostas() {
		return propostas;
	}

}
