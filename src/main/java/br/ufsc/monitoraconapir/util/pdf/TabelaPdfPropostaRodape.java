package br.ufsc.monitoraconapir.util.pdf;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;

public class TabelaPdfPropostaRodape extends PdfPTable {

	private static final int COLSPAN_DATA_GERACAO = 2;
	private static final int COLSPAN_ESPACO = 3;
	private static final int COLSPAN_PAGINA = 1;

	private static final Font FONTE = new Font(FontFamily.HELVETICA, 9);

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

	public TabelaPdfPropostaRodape(final Date dataGeracao, final Integer pagina) {
		super(new float[] { COLSPAN_DATA_GERACAO, COLSPAN_ESPACO, COLSPAN_PAGINA });

		setTotalWidth(557);
		getDefaultCell().setBorder(Rectangle.TOP);
		getDefaultCell().setVerticalAlignment(Element.ALIGN_BOTTOM);

		getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		addCell(PdfUtils.write(String.format("Gerado em %s", DATE_FORMAT.format(dataGeracao)), FONTE));

		addCell("");

		getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
		addCell(PdfUtils.write(String.format("pág %d", pagina), FONTE));
	}

}
