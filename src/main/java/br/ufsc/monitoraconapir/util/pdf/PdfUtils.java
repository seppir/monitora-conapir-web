package br.ufsc.monitoraconapir.util.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfUtils {

	public static final Font FONTE_NORMAL = new Font(FontFamily.HELVETICA, 10);
	public static final Font FONTE_NEGRITO = new Font(FontFamily.HELVETICA, 10, Font.BOLD);
	public static final Font FONTE_TITULO = new Font(FontFamily.HELVETICA, 12, Font.BOLD);

	public static Document createDocument(final File file) throws IOException, DocumentException {
		final Document pdf = new Document(PageSize.A4, 18, 18, 10, 30);
		final PdfWriter w = PdfWriter.getInstance(pdf, new FileOutputStream(file));
		w.setPageEvent(new PaginaPdf(new Date()));

		return pdf;
	}

	public static Phrase write(final String texto, final Font fonte) {
		return new Phrase(texto, fonte);
	}

	public static Phrase write(final String texto) {
		return write(texto, FONTE_NORMAL);
	}
}
