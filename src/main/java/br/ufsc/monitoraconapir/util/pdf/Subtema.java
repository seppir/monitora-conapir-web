package br.ufsc.monitoraconapir.util.pdf;

public enum Subtema {

	TODOS(0, "Todos") {
		@Override
		public String getResumo() {
			return getDescricao();
		}
	},
	SUBTEMA_1(1, "Estratégias para o desenvolvimento e o enfrentamento ao racismo"),
	SUBTEMA_2(2, "Políticas de igualdade racial no Brasil: avanços e desafios"),
	SUBTEMA_3(
		3,
		"Arranjos Institucionais para assegurar a sustentabilidade das políticas de igualdade racial: Sinapir, órgãos de promoção da igualdade racial, fórum de gestores, conselhos e ouvidorias"),
	SUBTEMA_4(
		4,
		"Participação política e controle social: igualdade racial nos espaços de decisão e mecanismos de participação da sociedade civil no monitoramento das políticas de igualdade racial");

	private final String descricao;
	private final Integer id;

	private Subtema(final Integer id, final String descricao) {
		this.id = id;
		this.descricao = descricao;
	}

	public Integer getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getResumo() {
		return String.format("%s - %s", getId(), getDescricao());
	}

	public static Subtema fromId(final Integer idSubtema) {
		switch(idSubtema) {
			case 1:
				return SUBTEMA_1;
			case 2:
				return SUBTEMA_2;
			case 3:
				return SUBTEMA_3;
			case 4:
				return SUBTEMA_4;
			default:
				return TODOS;
		}
	}

}
