package br.ufsc.monitoraconapir.util.pdf;

public enum FiltroPesquisa {

	TODAS("Todos (Resoluções Prioritárias e Não-Prioritárias)"),
	PRIORITARIAS("Resoluções Prioritárias");

	private Subtema subtema;
	private String filtro;
	private final String descricao;

	private FiltroPesquisa(final String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public Subtema getSubtema() {
		return subtema;
	}

	public void setSubtema(final Subtema subtema) {
		this.subtema = subtema;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(final String filtro) {
		this.filtro = filtro;
	}

}
