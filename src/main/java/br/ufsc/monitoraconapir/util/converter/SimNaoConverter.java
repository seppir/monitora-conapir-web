package br.ufsc.monitoraconapir.util.converter;

import javax.persistence.AttributeConverter;

public class SimNaoConverter implements AttributeConverter<Boolean, String> {

	@Override
	public String convertToDatabaseColumn(final Boolean attribute) {
		return attribute ? "S" : "N";
	}

	@Override
	public Boolean convertToEntityAttribute(final String dbData) {
		return "S".equals(dbData);
	}

}
