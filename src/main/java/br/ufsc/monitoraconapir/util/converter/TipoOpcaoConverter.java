package br.ufsc.monitoraconapir.util.converter;

import javax.persistence.AttributeConverter;

import br.ufsc.monitoraconapir.enumeration.TipoOpcao;

public class TipoOpcaoConverter implements AttributeConverter<TipoOpcao, String> {

	@Override
	public String convertToDatabaseColumn(final TipoOpcao attribute) {
		return attribute.name().substring(0);
	}

	@Override
	public TipoOpcao convertToEntityAttribute(final String dbData) {
		return TipoOpcao.converterDoBanco(dbData);
	}

}
