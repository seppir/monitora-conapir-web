package br.ufsc.monitoraconapir.util.converter;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang3.StringUtils;

public class FlagExibirConverter implements AttributeConverter<Boolean, String> {

	@Override
	public String convertToDatabaseColumn(final Boolean attribute) {
		return attribute ? null : "N";
	}

	@Override
	public Boolean convertToEntityAttribute(final String dbData) {
		return StringUtils.isBlank(dbData);
	}
}
