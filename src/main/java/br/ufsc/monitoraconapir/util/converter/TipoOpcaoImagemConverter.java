package br.ufsc.monitoraconapir.util.converter;

import javax.persistence.AttributeConverter;

import br.ufsc.monitoraconapir.enumeration.TipoOpcaoImagem;

public class TipoOpcaoImagemConverter implements AttributeConverter<TipoOpcaoImagem, Character> {

	@Override
	public Character convertToDatabaseColumn(final TipoOpcaoImagem attribute) {
		return attribute.getSigla();
	}

	@Override
	public TipoOpcaoImagem convertToEntityAttribute(final Character dbData) {
		return TipoOpcaoImagem.converterDoBanco(dbData);
	}

}
