package br.ufsc.monitoraconapir.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebAppInitializer implements WebApplicationInitializer {

	private static final String MAPPING_URL = "/";

	private static String URL_BASE;

	@Override
	public void onStartup(final ServletContext servletContext) throws ServletException {
		final WebApplicationContext context = getContext();
		servletContext.addListener(new ContextLoaderListener(context));
		final ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping(MAPPING_URL);
	}

	private WebApplicationContext getContext() {
		final AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.register(AppConfig.class, WebConfig.class);
		return context;
	}

	public static String getUrlBase() {
		return URL_BASE;
	}

	public static void setUrlBase(final String urlBase) {
		if(StringUtils.isBlank(URL_BASE))
			URL_BASE = urlBase;
	}

}
